import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

var config = {
  apiKey: "AIzaSyAg02ewnmsxcwPjHp9KuaMK53Atg0wkxFI",
  authDomain: "project-beer-jaa.firebaseapp.com",
  databaseURL: "https://project-beer-jaa.firebaseio.com",
  projectId: "project-beer-jaa",
  storageBucket: "project-beer-jaa.appspot.com",
  messagingSenderId: "768364048798"
};
firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

  export { database, auth, provider}